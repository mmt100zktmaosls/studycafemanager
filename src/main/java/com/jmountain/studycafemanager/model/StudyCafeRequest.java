package com.jmountain.studycafemanager.model;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class StudyCafeRequest {
    @NotNull
    private String customerName;
    @NotNull
    private String gender;
    @NotNull
    private int age;
    @NotNull
    private String phoneNumber;
    @NotNull
    private int theSeatNumber;
    @NotNull
    private Float lockerNumber;
    @NotNull
    private LocalDate dateOfUse;
    @NotNull
    private String remainingTime;
    @NotNull
    private String paymentAmountToday;
    @NotNull
    private String paymentAmountTotalDay;
    @NotNull
    private String dateAndTimeOfEntry;
    @NotNull
    private String dateAndTimeOfCheckOut;
}
