package com.jmountain.studycafemanager.service;

import com.jmountain.studycafemanager.entity.StudyCafeCustomer;
import com.jmountain.studycafemanager.model.StudyCafeRequest;
import com.jmountain.studycafemanager.repository.StudyCafeCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudyCafeCustomerService {

    private final StudyCafeCustomerRepository studyCafeCustomerRepository; // 서비스와 일을 하는 파일명, 서비스 파일안에서 불릴 명칭

    public void setStudyCafeCustomer(StudyCafeRequest request){
        StudyCafeCustomer addData = new StudyCafeCustomer();

        addData.setCustomerName(request.getCustomerName());
        addData.setAge(request.getAge());
        addData.setGender(request.getGender());
        addData.setLockerNumber(request.getLockerNumber());
        addData.setTheSeatNumber(request.getTheSeatNumber());
        addData.setDateAndTimeOfEntry(request.getDateAndTimeOfEntry());
        addData.setDateAndTimeOfCheckOut(request.getDateAndTimeOfCheckOut());
        addData.setPaymentAmountToday(request.getPaymentAmountToday());
        addData.setPaymentAmountTotalDay(request.getPaymentAmountTotalDay());
        addData.setRemainingTime(request.getRemainingTime());

        studyCafeCustomerRepository.save(addData);

    }
}
