package com.jmountain.studycafemanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter

public class StudyCafeCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String customerName;
    private String gender;
    private int age;
    private String phoneNumber;
    @Column(nullable = false)
    private int theSeatNumber;
    private Float lockerNumber;
    @Column(nullable = false, length = 20)
    private LocalDate dateOfUse;
    private String remainingTime;
    @Column(nullable = false, length = 20)
    private String paymentAmountToday;
    @Column(nullable = false, length = 20)
    private String paymentAmountTotalDay;
    @Column(nullable = false, length = 20)
    private String dateAndTimeOfEntry;
    @Column(nullable = false, length = 20)
    private String dateAndTimeOfCheckOut;

}
