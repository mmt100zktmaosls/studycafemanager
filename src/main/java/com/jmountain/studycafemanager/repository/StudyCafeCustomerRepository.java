package com.jmountain.studycafemanager.repository;

import com.jmountain.studycafemanager.entity.StudyCafeCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudyCafeCustomerRepository extends JpaRepository<StudyCafeCustomer, Long> {
}
