package com.jmountain.studycafemanager.controller;

import com.jmountain.studycafemanager.model.StudyCafeRequest;
import com.jmountain.studycafemanager.service.StudyCafeCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class StudyCafeController {
    private final StudyCafeCustomerService studyCafeCustomerService;
    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid  StudyCafeRequest request) {

        studyCafeCustomerService.setStudyCafeCustomer(request);

        return "OK";
    }


}